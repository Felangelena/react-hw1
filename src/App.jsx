import { useState } from 'react';
import './App.css';
import Modal from './modal/modal.jsx';
import Button from './btn/button.jsx';

function App() {
  const [openModal1, setOpenModal1] = useState(false);

  const openModalWin1 = () => {
    setOpenModal1(true);
  }

  const closeModalWin1 = () => {
      setOpenModal1(false);
  }

  const [openModal2, setOpenModal2] = useState(false);

  const openModalWin2 = () => {
    setOpenModal2(true);
  }

  const closeModalWin2 = () => {
      setOpenModal2(false);
  }

  return (
    <div className="App">
      <Button backgroundColor="purple" text="Open first modal" handleClick={openModalWin1}/>
      {openModal1 && <Modal header="The first modal" isCloseButton={true} text="Text for the first modal" close={closeModalWin1}
          actions={[
            <Button key="1" backgroundColor="maroon" text="Ok" handleClick={closeModalWin1}/>,
            <Button key="2" backgroundColor="maroon" text="Cancel" handleClick={closeModalWin1}/>
          ]} />
        }

      <Button backgroundColor="black" text="Open second modal" handleClick={openModalWin2}/>
      {openModal2 && <Modal header="The second modal" isCloseButton={false} text="Text for the second modal" close={closeModalWin2}
        actions={[
          <Button key="3" backgroundColor="firebrick" text="Ok" handleClick={closeModalWin2}/>,
          <Button key="4" backgroundColor="firebrick" text="Cancel" handleClick={closeModalWin2}/>
        ]} />
      }

    </div>
  );
}

export default App;
