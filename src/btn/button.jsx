import './button.css';

const Button = ({backgroundColor, text, handleClick}) => {

    return (
        <button type="button" className="btn"
        style={{backgroundColor: backgroundColor}}
        onClick={handleClick}>
        {text}
        </button>
    )
}

export default Button;